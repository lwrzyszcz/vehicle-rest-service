package pl.billennium.vehicles.controllers;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties.Headers;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.billennium.vehicles.dto.VehicleDTO;

import pl.billennium.vehicles.models.PagingContentWrapper;
import pl.billennium.vehicles.models.PagingModel;
import pl.billennium.vehicles.services.auth.AuthenticationService;
import pl.billennium.vehicles.services.vehicle.VehicleService;

import java.io.IOException;
import java.util.List;

@RestController
public class VehicleController {
    @Autowired
    private VehicleService vehicleService;
    @Autowired
    private AuthenticationService authenticationService;

    @CrossOrigin
    @GetMapping("/details/{id}")
    public ResponseEntity getVehiclesDetails(@PathVariable Long id, @RequestHeader("token") String token) {
    	if(!this.authenticationService.validateToken(token)) {
    		return new ResponseEntity("Unauthorized", HttpStatus.UNAUTHORIZED);
    	}
        VehicleDTO vehicle = this.vehicleService.getVehicleDetails(id);
        if (vehicle == null) {
            return new ResponseEntity("Vehicle with id: " + id + " doesn't exist in database", HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity(vehicle, HttpStatus.OK);
        }
    }

    @CrossOrigin
    @GetMapping("/showAll")
    public ResponseEntity getAllVehiclesDetails(@RequestHeader("token") String token) {
    	if(!this.authenticationService.validateToken(token)) {
    		return new ResponseEntity("Unauthorized", HttpStatus.UNAUTHORIZED);
    	}
        List<VehicleDTO> vehicles = this.vehicleService.getAllVehiclesDetails();
        if (vehicles == null) {
            return new ResponseEntity("UNKNOWN ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity(vehicles, HttpStatus.OK);
        }
    }

    @CrossOrigin
    @GetMapping("/show/{date}")
    public ResponseEntity getAllVehiclesDetailsWithBorrows(@PathVariable String date, @RequestHeader("token") String token) {
    	if(!this.authenticationService.validateToken(token)) {
    		return new ResponseEntity("Unauthorized", HttpStatus.UNAUTHORIZED);
    	}
        PagingContentWrapper<VehicleDTO> vehicles = this.vehicleService.getAllVehiclesDetailsWithBorrows(date, null);
        if (vehicles == null) {
            return new ResponseEntity("UNKNOWN ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity(vehicles, HttpStatus.OK);
        }
    }

    @CrossOrigin
    @GetMapping("/show/{date}/{paging}")
    public ResponseEntity getAllVehiclesDetailsWithBorrows(@PathVariable String date, @PathVariable String paging, @RequestHeader("token") String token) {
    	if(!this.authenticationService.validateToken(token)) {
    		return new ResponseEntity("Unauthorized", HttpStatus.UNAUTHORIZED);
    	}
        ObjectMapper om = new ObjectMapper();
        om.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        PagingModel pagination = null;
        try {
            pagination = om.readValue(paging, PagingModel.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        PagingContentWrapper<VehicleDTO> vehicles = this.vehicleService.getAllVehiclesDetailsWithBorrows(date, pagination);
        if (vehicles == null) {
            return new ResponseEntity("UNKNOWN ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity(vehicles, HttpStatus.OK);
        }
    }
    @CrossOrigin
    @DeleteMapping("delete/{id}")
    public ResponseEntity deleteVehicle(@PathVariable Long id, @RequestHeader("token") String token) {
    	if(!this.authenticationService.validateToken(token)) {
    		return new ResponseEntity("Unauthorized", HttpStatus.UNAUTHORIZED);
    	}
        this.vehicleService.deleteVehicle(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
