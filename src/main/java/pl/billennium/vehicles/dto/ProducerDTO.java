package pl.billennium.vehicles.dto;

import lombok.Getter;
import lombok.Setter;

public class ProducerDTO {
    @Getter
    @Setter
    private Long id;
    @Getter
    @Setter
    private String name;


}
