package pl.billennium.vehicles.dto;

import lombok.Getter;
import lombok.Setter;
import pl.billennium.vehicles.models.Color;

public class CarDTO {
    @Getter
    @Setter
    private Long id;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private Color color;
    @Getter
    @Setter
    private Long producerId;
    @Getter
    @Setter
    private String productionDate;


}
