package pl.billennium.vehicles.mappers;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.billennium.vehicles.dto.CarDTO;
import pl.billennium.vehicles.dto.EditCarDTO;
import pl.billennium.vehicles.entities.Car;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class CarMapper extends ModelMapper {

    @Autowired
    private Converters converters;

    @PostConstruct
    private void init() {
        typeMap(EditCarDTO.class, Car.class).addMappings(mapper -> mapper.using(converters.stringToLocalDateConverter).map(EditCarDTO::getProductionDate, Car::setProductionDate));
        typeMap(EditCarDTO.class, Car.class).addMappings(mapper -> mapper.using(converters.producerFromIdConverter).map(EditCarDTO::getProducerId, Car::setProducer));

        typeMap(CarDTO.class, Car.class).addMappings(mapper -> mapper.using(converters.stringToLocalDateConverter).map(CarDTO::getProductionDate, Car::setProductionDate));
        typeMap(CarDTO.class, Car.class).addMappings(mapper -> mapper.using(converters.producerFromIdConverter).map(CarDTO::getProducerId, Car::setProducer));

        typeMap(Car.class, CarDTO.class).addMappings(mapper -> mapper.using(converters.localDateTostringConverter).map(Car::getProductionDate, CarDTO::setProductionDate));
        typeMap(Car.class, CarDTO.class).addMappings(mapper -> mapper.using(converters.producerIdConverter).map(Car::getProducer, CarDTO::setProducerId));

        getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

    }

    public <S,D> List<D> maplist(List<S> source, Class<D> destinationType) {
        List<D> destination = new ArrayList<>();
        if(source != null) {
            source.forEach(element ->{
                destination.add(map(element, destinationType));
            });
        }
        return destination;
    }

}
