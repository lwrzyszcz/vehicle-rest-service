package pl.billennium.vehicles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.annotation.Transactional;
import org.testng.Assert;
import org.testng.annotations.Test;
import pl.billennium.vehicles.entities.Bike;
import pl.billennium.vehicles.entities.Car;
import pl.billennium.vehicles.models.Color;
import pl.billennium.vehicles.repositories.BikeRepository;
import pl.billennium.vehicles.repositories.CarRepository;
import pl.billennium.vehicles.repositories.VehicleRepository;



@SpringBootTest
@Transactional
public class VehicleTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private CarRepository carRepository;
    @Autowired
    private BikeRepository bikeRepository;
    @Autowired
    private VehicleRepository vehicleRepository;

    @Test
    public void testVehicles() {
        Car car = new Car("Ford", Color.zielony);
        this.carRepository.save(car);
        Assert.assertNotNull(this.carRepository.findAll());

        Bike bike = new Bike(Math.round(Math.random()*100000000));
        this.bikeRepository.save(bike);

        Assert.assertNotNull(this.bikeRepository.findAll());

    }


}
