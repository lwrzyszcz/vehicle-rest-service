package pl.billennium.vehicles.services.borrow;

import org.springframework.web.bind.annotation.RequestBody;
import pl.billennium.vehicles.dto.BorrowDTO;
import pl.billennium.vehicles.dto.BorrowExtendedDTO;
import pl.billennium.vehicles.dto.BorrowerDTO;

import java.util.List;

public interface BorrowService {
    /**
     * Will add new borrower to database
     * @param borrower
     * @return
     */
    BorrowerDTO addBorrower(@RequestBody BorrowerDTO borrower);

    /**
     * Will borrow vehicle to borrower that is specified in BorrowDTO
     * @param borrow DTO with info about borrower, vehicle and borrow date
     * @return
     */
    BorrowDTO borrowVehicle(@RequestBody BorrowDTO borrow);

    /**
     *
     * @return all borrows
     */
    List<BorrowExtendedDTO> getAllBorrows();

    /**
     *
     * @return all borrowers
     */
    List<BorrowerDTO> getAllBorrowers();

    /**
     * Method deleteBorrower will delete borrower with specified id from database
     * @param id borrower id
     * @return
     */
    boolean deleteBorrower(Long id);

    /**
     * Method deleteBorrow will delete borrow with specified id from database
     * @param id borrow id
     * @return
     */
    boolean deleteBorrow(Long id);
}
