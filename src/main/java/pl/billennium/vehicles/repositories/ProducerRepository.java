package pl.billennium.vehicles.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.billennium.vehicles.entities.Producer;


@Repository
public interface ProducerRepository extends JpaRepository<Producer, Long> {
}
