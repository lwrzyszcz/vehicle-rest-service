package pl.billennium.vehicles.dto;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class BorrowDTO {

    private String borrowDate;

    private Long vehicleId;

    private BorrowerDTO borrower;

}
