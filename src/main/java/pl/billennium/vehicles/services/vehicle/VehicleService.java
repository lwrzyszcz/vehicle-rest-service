package pl.billennium.vehicles.services.vehicle;

import pl.billennium.vehicles.dto.VehicleDTO;
import pl.billennium.vehicles.models.PagingContentWrapper;
import pl.billennium.vehicles.models.PagingModel;

import java.util.List;

public interface VehicleService {
    /**
     * Vehicle details by vehicle id
     * @param id
     * @return vehicle with given id
     */
    VehicleDTO getVehicleDetails(Long id);

    /**
     * Vehicles without paging
     * @return all vehicles - without paging
     */
    List<VehicleDTO> getAllVehiclesDetails();

    /**
     * Vehicles with paging
     * @param date date to search borrow
     * @param paging Param has info about selected page and page size
     * @return all vehicles for page, that was specified in param paging
     */
    PagingContentWrapper<VehicleDTO> getAllVehiclesDetailsWithBorrows(String date, PagingModel paging);

    /**
     * deleteVehicle method will delete vehicle with specified id
     * @param id
     * @return
     */
    boolean deleteVehicle(Long id);
}
