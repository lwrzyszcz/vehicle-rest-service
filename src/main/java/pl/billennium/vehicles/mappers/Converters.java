package pl.billennium.vehicles.mappers;

import org.modelmapper.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.billennium.vehicles.dto.BorrowDTO;
import pl.billennium.vehicles.dto.VehicleDTO;
import pl.billennium.vehicles.entities.Borrow;
import pl.billennium.vehicles.entities.Borrower;
import pl.billennium.vehicles.entities.Producer;
import pl.billennium.vehicles.entities.Vehicle;
import pl.billennium.vehicles.repositories.BorrowRepository;
import pl.billennium.vehicles.repositories.BorrowerRepository;
import pl.billennium.vehicles.repositories.ProducerRepository;
import pl.billennium.vehicles.repositories.VehicleRepository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
public class Converters {

    @Autowired
    private ProducerRepository producerRepository;
    @Autowired
    private BorrowRepository borrowRepository;
    @Autowired
    private VehicleRepository vehicleRepository;
    @Autowired
    private BorrowerRepository borrowerRepository;
    @Autowired
    private BorrowMapper borrowMapper;
    @Autowired
    private VehicleMapper vehicleMapper;


    Converter<LocalDate, String> localDateTostringConverter = mappingContext -> {
        LocalDate source = mappingContext.getSource();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String formattedString = source.format(formatter);
        return formattedString;
    };

    Converter<Producer, Long> producerIdConverter = mappingContext -> {
        Producer source = mappingContext.getSource();
        return source != null ? source.getId() : null;
    };

    Converter<Borrow, Long> borrowIdConverter = mappingContext -> {
        Borrow source = mappingContext.getSource();
        return source.getId();
    };

    Converter<Borrower, Long> borrowerIdConverter = mappingContext -> {
        Borrower source = mappingContext.getSource();
        return source.getId();
    };

    Converter<Vehicle, Long> vehicleIdConverter = mappingContext -> {
        Vehicle source = mappingContext.getSource();
        return source.getId();
    };

    Converter<Vehicle, VehicleDTO> vehicleToVehicleDTO = mappingContext -> {
        Vehicle source = mappingContext.getSource();
        return this.vehicleMapper.map(source, VehicleDTO.class);
    };

    Converter<String, LocalDate> stringToLocalDateConverter = mappingContext -> {
        String source = mappingContext.getSource();
        return LocalDate.parse(source);
    };

    Converter<Long, Producer> producerFromIdConverter = mappingContext -> {
        Long source = mappingContext.getSource();
        return this.producerRepository.findOne(source);
    };

    Converter<Long, Borrow> borrowFromIdConverter = mappingContext -> {
        Long source = mappingContext.getSource();
        return this.borrowRepository.findOne(source);
    };

    Converter<Long, Borrower> borrowerFromIdConverter = mappingContext -> {
        Long source = mappingContext.getSource();
        return this.borrowerRepository.findOne(source);
    };

    Converter<Long, Vehicle> vehicleFromIdConverter = mappingContext -> {
        Long source = mappingContext.getSource();
        return this.vehicleRepository.findOne(source);
    };

    Converter<List<Borrow>, Boolean> wasBorrowedConverter = mappingContext -> {
        List<Borrow> source = mappingContext.getSource();
        return source != null && source.size() > 0;
    };

    public Converter<List<Borrow>, BorrowDTO> getBorrowedAtDayConverter(LocalDate borrowDate) {
        Converter<List<Borrow>, BorrowDTO> borrowedAtDayConverter = mappingContext -> {
            List<Borrow> source = mappingContext.getSource();
            BorrowDTO foundedBorrow = null;
            if (source != null) {
                for (Borrow borrow : source) {
                    if (borrow.getBorrowDate().isEqual(borrowDate)) {
                        foundedBorrow = borrowMapper.map(borrow, BorrowDTO.class);
                    }
                }
            }
            return foundedBorrow;
        };
        return borrowedAtDayConverter;
    }

}
