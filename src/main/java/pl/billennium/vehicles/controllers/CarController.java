package pl.billennium.vehicles.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.billennium.vehicles.dto.CarDTO;
import pl.billennium.vehicles.dto.EditCarDTO;
import pl.billennium.vehicles.services.car.CarService;


@RestController
public class CarController {

    @Autowired
    private CarService carService;

    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @CrossOrigin
    @PostMapping(value = "/addCar")
    public ResponseEntity addCar(@RequestBody CarDTO car) {
        CarDTO addedCar = this.carService.addCar(car);
        if (addedCar == null) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity(addedCar, HttpStatus.OK);
        }
    }
    @CrossOrigin
    @PutMapping(value = "/editCar")
    public ResponseEntity editCar(@RequestBody EditCarDTO car) {
        CarDTO addedCar = this.carService.editCar(car);
        if (addedCar == null) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity(addedCar, HttpStatus.OK);
        }
    }



}
