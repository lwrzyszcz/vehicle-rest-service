package pl.billennium.vehicles.mappers;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.billennium.vehicles.dto.BorrowDTO;
import pl.billennium.vehicles.dto.BorrowExtendedDTO;
import pl.billennium.vehicles.entities.Borrow;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class BorrowMapper extends ModelMapper {

    @Autowired
    private Converters converters;

    @PostConstruct
    private void init() {

        typeMap(Borrow.class, BorrowDTO.class).addMappings(mapper -> mapper.map(Borrow::getBorrower, BorrowDTO::setBorrower));
        typeMap(Borrow.class, BorrowDTO.class).addMappings(mapper -> mapper.using(converters.vehicleIdConverter).map(Borrow::getVehicle, BorrowDTO::setVehicleId));

        typeMap(Borrow.class, BorrowExtendedDTO.class).addMappings(mapper -> mapper.map(Borrow::getBorrower, BorrowExtendedDTO::setBorrower));
        typeMap(Borrow.class, BorrowExtendedDTO.class).addMappings(mapper -> mapper.using(converters.vehicleToVehicleDTO).map(Borrow::getVehicle, BorrowExtendedDTO::setVehicle));
        getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    }

    public <S,D> List<D> maplist(List<S> source, Class<D> destinationType) {
        List<D> destination = new ArrayList<>();
        if(source != null) {
            source.forEach(element ->{
                destination.add(map(element, destinationType));
            });
        }
        return destination;
    }
}
