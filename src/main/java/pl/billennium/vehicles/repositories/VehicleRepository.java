package pl.billennium.vehicles.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.billennium.vehicles.entities.Vehicle;

import java.util.List;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long> {

    @Query(value = "select count(*) from Vehicle v",
            nativeQuery = true)
    int getCountQuery();

    @Query(value = "select v from Vehicle v  Left JOIN FETCH v.borrows b left join fetch v.producer left join fetch b.borrower order by v.id asc", countQuery = "select count(v) from Vehicle v")
    Page<Vehicle> getAllVehicleDetails(Pageable pageable);

    @Query("select v from Vehicle v  Left JOIN FETCH v.borrows b left join fetch v.producer left join fetch b.borrower where v.id = :id")
    Vehicle findVehicleById(@Param("id") Long id);

    @Query("select v from Vehicle v  Left JOIN FETCH v.borrows b left join fetch v.producer left join fetch b.borrower order by v.id asc")
    List<Vehicle> findAllVehicleModel();



}
