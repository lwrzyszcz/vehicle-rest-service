package pl.billennium.vehicles.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.billennium.vehicles.entities.Borrow;

import java.util.List;

@Repository
public interface BorrowRepository extends JpaRepository<Borrow, Long> {

    @Query("Select b from Borrow b join fetch b.vehicle v join fetch b.borrower left join fetch v.borrows left join fetch v.producer")
    List<Borrow> getAllBorrows();

}
