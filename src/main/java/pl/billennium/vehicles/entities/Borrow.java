package pl.billennium.vehicles.entities;

import lombok.Getter;
import lombok.Setter;
import pl.billennium.vehicles.entities.converters.LocalDateToLongConverter;

import javax.persistence.*;
import java.time.LocalDate;
@Getter
@Setter
@Entity
public class Borrow {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @OneToOne
    private Borrower borrower;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "vehicle_id")
    private Vehicle vehicle;
    @Convert(converter = LocalDateToLongConverter.class)
    private LocalDate borrowDate;

}
