package pl.billennium.vehicles.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BorrowExtendedDTO {

    private Long id;

    private String borrowDate;

    private VehicleDTO vehicle;

    private BorrowerDTO borrower;
}
