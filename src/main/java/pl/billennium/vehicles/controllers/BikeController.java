package pl.billennium.vehicles.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.billennium.vehicles.dto.BikeDTO;
import pl.billennium.vehicles.services.bike.BikeService;

@RestController
public class BikeController {

    @Autowired
    private BikeService bikeService;

    @CrossOrigin
    @PostMapping("/addBike/{number}")
    public ResponseEntity putBikeWithNumber(@PathVariable Long number) {
        BikeDTO added = this.bikeService.putBikeWithNumber(number);
        return new ResponseEntity(added, HttpStatus.OK);
    }
}
