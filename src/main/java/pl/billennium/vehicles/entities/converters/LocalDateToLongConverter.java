package pl.billennium.vehicles.entities.converters;

import javax.persistence.AttributeConverter;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;

public class LocalDateToLongConverter implements AttributeConverter<LocalDate, Long> {
    @Override
    public Long convertToDatabaseColumn(LocalDate localDate) {
        if(localDate == null) {
            localDate = LocalDate.now();
        }
        return localDate.atStartOfDay(ZoneId.systemDefault()).toEpochSecond();
    }

    @Override
    public LocalDate convertToEntityAttribute(Long aLong) {
        if(aLong == null) {
            aLong = 0L;
        }
        LocalDate localDate = Instant.ofEpochSecond(aLong).atZone(ZoneId.systemDefault()).toLocalDate();
        return localDate;
    }
}