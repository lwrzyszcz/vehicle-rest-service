package pl.billennium.vehicles.services.bike;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import pl.billennium.vehicles.dto.BikeDTO;
import pl.billennium.vehicles.entities.Bike;
import pl.billennium.vehicles.mappers.BikeMapper;
import pl.billennium.vehicles.repositories.BikeRepository;

import java.util.List;

@Service
public class BikeServiceImpl implements BikeService {
    @Autowired
    private BikeRepository bikeRepository;

    @Autowired
    private BikeMapper bikeMapper;

    @Override
    public BikeDTO putBikeWithNumber(@PathVariable Long number) {
        Bike bike = new Bike(number);
        Bike added = this.bikeRepository.save(bike);
        return bikeMapper.map(added, BikeDTO.class);
    }

    @Override
    public List<BikeDTO> getAllBikes() {
        List<Bike> bikes = bikeRepository.findAll();
        return this.bikeMapper.maplist(bikes, BikeDTO.class);
    }
}
