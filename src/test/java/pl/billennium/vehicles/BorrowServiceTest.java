package pl.billennium.vehicles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.annotation.Transactional;
import org.testng.Assert;
import org.testng.annotations.Test;
import pl.billennium.vehicles.dto.*;
import pl.billennium.vehicles.services.bike.BikeService;
import pl.billennium.vehicles.services.borrow.BorrowService;
import pl.billennium.vehicles.services.vehicle.VehicleService;


@SpringBootTest
@Transactional
public class BorrowServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private BorrowService borrowService;

    @Autowired
    private VehicleService vehicleService;

    @Autowired
    private BikeService bikeService;

    @Test
    public void addBorrowerTest() {
        BorrowerDTO borrower = new BorrowerDTO();
        borrower.setName("Borrower 4");

        BorrowerDTO added = borrowService.addBorrower(borrower);
        Assert.assertNotNull(added);
        Assert.assertNotNull(this.borrowService.getAllBorrowers());
    }

    @Test
    public void addBorrowTest() {
        BikeDTO vehicle = this.bikeService.putBikeWithNumber(Math.round(Math.random()*100000000));
        BorrowerDTO borrower = new BorrowerDTO();
        borrower.setName("Borrower 2");

        borrower = borrowService.addBorrower(borrower);
        BorrowDTO borrow = new BorrowDTO();
        borrow.setBorrowDate("2018-08-14");
        borrow.setBorrower(borrower);
        borrow.setVehicleId(vehicle.getId());
        BorrowDTO added = this.borrowService.borrowVehicle(borrow);

        Assert.assertNotNull(added);
    }
}
