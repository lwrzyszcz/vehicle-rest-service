package pl.billennium.vehicles.services.vehicle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import pl.billennium.vehicles.dto.VehicleDTO;
import pl.billennium.vehicles.entities.Vehicle;
import pl.billennium.vehicles.mappers.VehicleMapper;
import pl.billennium.vehicles.models.PagingContentWrapper;
import pl.billennium.vehicles.models.PagingModel;
import pl.billennium.vehicles.repositories.VehicleRepository;

import java.util.List;

@Service
public class VehicleServiceImpl implements VehicleService {
    @Autowired
    private VehicleRepository vehicleRepository;
    @Autowired
    private VehicleMapper vehicleMapper;

    @Override
    public VehicleDTO getVehicleDetails(Long id) {
        Vehicle vehicle = this.vehicleRepository.findVehicleById(id);
        return vehicleMapper.map(vehicle, VehicleDTO.class);
    }

    @Override
    public List<VehicleDTO> getAllVehiclesDetails() {
        List<Vehicle> vehicles = this.vehicleRepository.findAllVehicleModel();
        return this.vehicleMapper.maplist(vehicles, VehicleDTO.class);
    }

    @Override
    public PagingContentWrapper<VehicleDTO> getAllVehiclesDetailsWithBorrows(String date, PagingModel paging) {
        Page<Vehicle> vehicles = this.vehicleRepository.getAllVehicleDetails(paging);
    this.vehicleMapper.setDate("date", date);
        List<VehicleDTO> vehicleDTOS = this.vehicleMapper.maplist(vehicles.getContent(), VehicleDTO.class);
        PagingContentWrapper<VehicleDTO> p = new PagingContentWrapper();
        p.setContent(vehicleDTOS);
        p.setNumber(vehicles.getNumber());
        p.setTotalElements((int) vehicles.getTotalElements());
        p.setTotalPages(vehicles.getTotalPages());
        return p;
    }

    @Override
    public boolean deleteVehicle(Long id) {
        this.vehicleRepository.delete(id);
        return true;
    }
}
