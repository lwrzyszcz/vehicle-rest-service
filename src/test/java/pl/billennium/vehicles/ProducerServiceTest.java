package pl.billennium.vehicles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.annotation.Transactional;
import org.testng.Assert;
import org.testng.annotations.Test;
import pl.billennium.vehicles.dto.ProducerDTO;
import pl.billennium.vehicles.services.producer.ProducerService;

@SpringBootTest
@Transactional
public class ProducerServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private ProducerService producerService;

    @Test
    public void addProducerTest() {
        ProducerDTO producer = new ProducerDTO();
        producer.setName("BMW");

        assert producerService.addProducer(producer) != null;
        Assert.assertNotNull(this.producerService.getAllProducers());
    }
}
