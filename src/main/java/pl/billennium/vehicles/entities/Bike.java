package pl.billennium.vehicles.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Digits;

@Getter
@Setter
@Entity
public class Bike extends Vehicle {

    @Digits(integer = 8, fraction = 0)
    @Column(unique =  true)
    private Long bikeNumber;

    public Bike() {
    }

    public Bike(Long bikeNumber) {
        this.bikeNumber = bikeNumber;
    }

}
