package pl.billennium.vehicles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.annotation.Transactional;
import org.testng.Assert;
import org.testng.annotations.Test;
import pl.billennium.vehicles.dto.BikeDTO;
import pl.billennium.vehicles.services.bike.BikeService;

@SpringBootTest
@Transactional
public class BikeServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private BikeService bikeService;

    @Test
    public void addBikeTest() {

        BikeDTO bike = this.bikeService.putBikeWithNumber(Math.round(Math.random()*100000000));

        Assert.assertNotNull(bike);

        Assert.assertNotNull(bikeService.getAllBikes());
    }
}
