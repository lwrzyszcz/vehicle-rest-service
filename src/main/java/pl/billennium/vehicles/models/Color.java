package pl.billennium.vehicles.models;

public enum Color {
    czerwony("R"), zielony("G"), niebieski("B"),brak("E");
    private String color;

    Color(String color){
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public static Color getColor(String color){
        for (Color color1 : Color.values()) {
            if (color1.getColor().equals(color)) {
                return color1;
            }
        }
        return brak;
    }
}
