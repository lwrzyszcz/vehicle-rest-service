package pl.billennium.vehicles.mappers;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProducerMapper extends ModelMapper {

    @PostConstruct
    private void init() {
        getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    }

    public <S,D> List<D> maplist(List<S> source, Class<D> destinationType) {
        List<D> destination = new ArrayList<>();
        if(source != null) {
            source.forEach(element ->{
                destination.add(map(element, destinationType));
            });
        }
        return destination;
    }
}
