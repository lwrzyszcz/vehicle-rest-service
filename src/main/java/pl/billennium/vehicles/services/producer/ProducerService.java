package pl.billennium.vehicles.services.producer;

import pl.billennium.vehicles.dto.ProducerDTO;

import java.util.List;

public interface ProducerService {
    /**
     *
     * @param id
     * @return producer with specified id
     */
    ProducerDTO getProducerById(Long id);

    /**
     *  Method adds new producer to database
     * @param producer
     * @return
     */
    ProducerDTO addProducer(ProducerDTO producer);

    /**
     *
     * @return all producers from database
     */
    List<ProducerDTO> getAllProducers();

    /**
     * That method will delete producer with specified id
     * @param id
     * @return
     */
    boolean deleteProducer(Long id);
}
