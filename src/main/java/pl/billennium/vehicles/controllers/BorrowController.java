package pl.billennium.vehicles.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.billennium.vehicles.dto.BorrowDTO;
import pl.billennium.vehicles.dto.BorrowExtendedDTO;
import pl.billennium.vehicles.dto.BorrowerDTO;
import pl.billennium.vehicles.services.borrow.BorrowService;

import java.util.List;

@RestController
public class BorrowController {

    @Autowired
    private BorrowService borrowService;

    @CrossOrigin
    @PostMapping(value = "/addBorrower")
    public ResponseEntity addBorrower(@RequestBody BorrowerDTO borrower) {
        BorrowerDTO addedBorrower = this.borrowService.addBorrower(borrower);
        if (addedBorrower == null) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity(addedBorrower, HttpStatus.OK);
        }
    }

    @CrossOrigin
    @PostMapping(value = "/borrow")
    public ResponseEntity borrowVehicle(@RequestBody BorrowDTO borrow) {
        BorrowDTO addedBorrow = this.borrowService.borrowVehicle(borrow);
        return new ResponseEntity(addedBorrow, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/allBorrows")
    public ResponseEntity getBorrows() {
        List<BorrowExtendedDTO> borrowers = this.borrowService.getAllBorrows();
        return new ResponseEntity(borrowers, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/allBorrowers")
    public ResponseEntity getBorrowers() {
        List<BorrowerDTO> borrowers = this.borrowService.getAllBorrowers();
        return new ResponseEntity(borrowers, HttpStatus.OK);
    }

    @CrossOrigin
    @DeleteMapping("/deleteBorrower/{id}")
    public ResponseEntity deleteBorrower(@PathVariable Long id) {
        boolean removed = this.borrowService.deleteBorrower(id);
        return new ResponseEntity(removed, HttpStatus.OK);
    }

    @CrossOrigin
    @DeleteMapping("/deleteBorrow/{id}")
    public ResponseEntity deleteBorrow(@PathVariable Long id) {
        boolean removed = this.borrowService.deleteBorrow(id);
        return new ResponseEntity(removed, HttpStatus.OK);
    }
}
