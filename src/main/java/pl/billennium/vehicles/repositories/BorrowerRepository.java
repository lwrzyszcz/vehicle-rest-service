package pl.billennium.vehicles.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.billennium.vehicles.entities.Borrower;

@Repository
public interface BorrowerRepository extends JpaRepository<Borrower, Long> {
}
