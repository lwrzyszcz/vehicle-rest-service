package pl.billennium.vehicles.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.billennium.vehicles.dto.ProducerDTO;
import pl.billennium.vehicles.services.producer.ProducerService;

import java.util.List;

@RestController
public class ProducerController {

    @Autowired
    private ProducerService producerService;

    @CrossOrigin
    @PostMapping(value = "/addProducer")
    public ResponseEntity addBorrower(@RequestBody ProducerDTO producer) {
        ProducerDTO addedBorrower = this.producerService.addProducer(producer);
        if (addedBorrower == null) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity(addedBorrower, HttpStatus.OK);
        }
    }

    @CrossOrigin
    @GetMapping("/allProducers")
    public ResponseEntity getAllVehiclesDetails() {
        List<ProducerDTO> producers = this.producerService.getAllProducers();
        if (producers == null) {
            return new ResponseEntity("UNKNOWN ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
        } else {
            return new ResponseEntity(producers, HttpStatus.OK);
        }
    }

    @CrossOrigin
    @DeleteMapping("/deleteProducer/{id}")
    public ResponseEntity deleteProducer(@PathVariable Long id) {
        boolean removed = this.producerService.deleteProducer(id);
        return new ResponseEntity(removed, HttpStatus.OK);
    }

}
