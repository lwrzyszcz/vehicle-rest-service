package pl.billennium.vehicles;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.annotation.Transactional;
import org.testng.Assert;
import org.testng.annotations.Test;
import pl.billennium.vehicles.dto.CarDTO;
import pl.billennium.vehicles.dto.EditCarDTO;
import pl.billennium.vehicles.dto.ProducerDTO;
import pl.billennium.vehicles.models.Color;
import pl.billennium.vehicles.services.car.CarService;
import pl.billennium.vehicles.services.producer.ProducerService;

import java.util.List;

@SpringBootTest
@Transactional
public class CarServiceTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private CarService carService;

    @Autowired
    private ProducerService producerService;

    @Test
    public void addCarTest() {
        ProducerDTO producer = new ProducerDTO();
        producer.setName("Opel");
        ProducerDTO addedP = this.producerService.addProducer(producer);
        Assert.assertNotNull(addedP);

        CarDTO car = new CarDTO();
        car.setName("Vectra C");
        car.setProductionDate("2005-12-12");
        car.setColor(Color.zielony);
        car.setProducerId(addedP.getId());

        this.carService.addCar(car);

        Assert.assertNotNull(this.carService.getAllCars());
        assert this.carService.getAllCars().size() > 0;
    }

    @Test
    public void editCarTest() {
        List<ProducerDTO> producers = producerService.getAllProducers();
        List<CarDTO> cars = carService.getAllCars();

        Assert.assertNotNull(cars);
        assert cars.size() > 0;

        Assert.assertNotNull(producers);
        assert producers.size() > 0;

        EditCarDTO edited = new EditCarDTO();
        edited.setId(cars.get(0).getId());
        edited.setColor(Color.czerwony);
        edited.setProductionDate("2003-11-11");
        edited.setName("Opel Vectra");
        edited.setProducerId(producers.get(0).getId());
        Assert.assertNotNull(this.carService.editCar(edited));


    }
}
