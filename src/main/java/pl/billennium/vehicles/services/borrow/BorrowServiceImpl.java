package pl.billennium.vehicles.services.borrow;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import pl.billennium.vehicles.dto.BorrowDTO;
import pl.billennium.vehicles.dto.BorrowExtendedDTO;
import pl.billennium.vehicles.dto.BorrowerDTO;
import pl.billennium.vehicles.entities.Borrow;
import pl.billennium.vehicles.entities.Borrower;
import pl.billennium.vehicles.entities.Vehicle;
import pl.billennium.vehicles.mappers.BorrowMapper;
import pl.billennium.vehicles.mappers.BorrowerMapper;
import pl.billennium.vehicles.repositories.BorrowRepository;
import pl.billennium.vehicles.repositories.BorrowerRepository;
import pl.billennium.vehicles.repositories.VehicleRepository;

import java.time.LocalDate;
import java.util.List;

@Service
public class BorrowServiceImpl implements BorrowService {

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private BorrowerRepository borrowerRepository;

    @Autowired
    private BorrowRepository borrowRepository;

    @Autowired
    private BorrowerMapper borrowerMapper;

    @Autowired
    private BorrowMapper borrowMapper;

    @Override
    public BorrowerDTO addBorrower(@RequestBody BorrowerDTO borrower) {
        Borrower mappedBorrower = borrowerMapper.map(borrower, Borrower.class);
        BorrowerDTO addedBorrower = borrowerMapper.map(this.borrowerRepository.save(mappedBorrower), BorrowerDTO.class);
        return addedBorrower;
    }

    @Override
    public BorrowDTO borrowVehicle(@RequestBody BorrowDTO borrow) {
        Borrower borrower = this.borrowerRepository.findOne(borrow.getBorrower().getId());
        Vehicle vehicle = this.vehicleRepository.findOne(borrow.getVehicleId());
        Borrow newBorrow = new Borrow();
        newBorrow.setBorrowDate(LocalDate.parse(borrow.getBorrowDate()));
        newBorrow.setBorrower(borrower);
        newBorrow.setVehicle(vehicle);
        BorrowDTO addedBorrow = borrowerMapper.map(this.borrowRepository.save(newBorrow), BorrowDTO.class);
        return addedBorrow;
    }

    @Override
    public List<BorrowExtendedDTO> getAllBorrows() {
        List<Borrow> borrows = borrowRepository.getAllBorrows();
        return this.borrowMapper.maplist(borrows, BorrowExtendedDTO.class);
    }

    @Override
    public List<BorrowerDTO> getAllBorrowers() {
        List<Borrower> producers = borrowerRepository.findAll();
        return this.borrowerMapper.maplist(producers, BorrowerDTO.class);
    }

    @Override
    public boolean deleteBorrower(Long id){
        this.borrowerRepository.delete(id);
        return true;
    }

    @Override
    public boolean deleteBorrow(Long id){
        this.borrowRepository.delete(id);
        return true;
    }

}
