package pl.billennium.vehicles.mappers;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.billennium.vehicles.dto.VehicleDTO;
import pl.billennium.vehicles.entities.Bike;
import pl.billennium.vehicles.entities.Car;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class VehicleMapper extends ModelMapper {

    @Autowired
    private Converters converters;
    @Autowired
    private ProducerMapper producerMapper;

    private Map<String, Object> properties = new HashMap<>();

    @PostConstruct
    private void init() {
        typeMap(Car.class, VehicleDTO.class).addMappings(mapper -> mapper.using(converters.localDateTostringConverter).map(Car::getProductionDate, VehicleDTO::setProductionDate));
        typeMap(Car.class, VehicleDTO.class).addMappings(mapper -> mapper.map(Car::getProducer, VehicleDTO::setProducer));
        typeMap(Bike.class, VehicleDTO.class).addMappings(mapper -> mapper.map(Bike::getDtype, VehicleDTO::setType));
        typeMap(Car.class, VehicleDTO.class).addMappings(mapper -> mapper.map(Car::getDtype, VehicleDTO::setType));
        typeMap(Car.class, VehicleDTO.class).addMappings(mapper -> mapper.using(converters.wasBorrowedConverter).map(Car::getBorrows, VehicleDTO::setBorrowed));
        typeMap(Bike.class, VehicleDTO.class).addMappings(mapper -> mapper.using(converters.wasBorrowedConverter).map(Bike::getBorrows, VehicleDTO::setBorrowed));

        getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

    }

    public <S,D> List<D>  maplist(List<S> source, Class<D> destinationType) {
        List<D> destination = new ArrayList<>();
        if(source != null) {
            source.forEach(element ->{
                destination.add(map(element, destinationType));
            });
        }
        return destination;
    }

    public void setDate(String property, Object value){
        this.properties.put(property, value);
        typeMap(Car.class, VehicleDTO.class).addMappings(mapper -> mapper.using(converters.getBorrowedAtDayConverter(LocalDate.parse((String)properties.get(property)))).map(Car::getBorrows, VehicleDTO::setBorrow));
        typeMap(Bike.class, VehicleDTO.class).addMappings(mapper -> mapper.using(converters.getBorrowedAtDayConverter(LocalDate.parse((String)properties.get(property)))).map(Bike::getBorrows, VehicleDTO::setBorrow));

    }
}
