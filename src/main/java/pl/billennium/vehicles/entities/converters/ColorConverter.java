package pl.billennium.vehicles.entities.converters;


import pl.billennium.vehicles.models.Color;

import javax.persistence.AttributeConverter;

public class ColorConverter implements AttributeConverter<Color, String> {
    @Override
    public String convertToDatabaseColumn(Color color) {
        return color.getColor();
    }

    @Override
    public Color convertToEntityAttribute(String s) {
        return Color.getColor(s);
    }
}
