package pl.billennium.vehicles.services.car;

import org.springframework.web.bind.annotation.RequestBody;
import pl.billennium.vehicles.dto.CarDTO;
import pl.billennium.vehicles.dto.EditCarDTO;

import java.util.List;

public interface CarService {
    /**
     * Adding new car
     * @param car
     * @return
     */
    CarDTO addCar(@RequestBody CarDTO car);

    /**
     * Updating existing car -  required car's id
     * @param car
     * @return
     */
    CarDTO editCar(@RequestBody EditCarDTO car);

    /**
     *
     * @return all vehicles with type car
     */
    List<CarDTO> getAllCars();
}
