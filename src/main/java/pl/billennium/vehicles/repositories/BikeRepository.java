package pl.billennium.vehicles.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.billennium.vehicles.entities.Bike;

@Repository
public interface BikeRepository extends JpaRepository<Bike, Long> {
}
