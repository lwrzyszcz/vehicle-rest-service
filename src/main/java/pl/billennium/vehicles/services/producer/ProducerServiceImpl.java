package pl.billennium.vehicles.services.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.billennium.vehicles.dto.ProducerDTO;
import pl.billennium.vehicles.entities.Producer;
import pl.billennium.vehicles.mappers.ProducerMapper;
import pl.billennium.vehicles.repositories.ProducerRepository;

import java.util.List;

@Service
public class ProducerServiceImpl implements ProducerService {

    @Autowired
    private ProducerRepository producerRepository;

    @Autowired
    private ProducerMapper producerMapper;


    @Override
    public ProducerDTO getProducerById(Long id) {
        Producer producer = this.producerRepository.findOne(id);
        return this.producerMapper.map(producer, ProducerDTO.class);
    }

    @Override
    public ProducerDTO addProducer(ProducerDTO producer) {
        Producer addedProducer = this.producerRepository.save(producerMapper.map(producer,Producer.class));
        return this.producerMapper.map(addedProducer, ProducerDTO.class);
    }

    @Override
    public List<ProducerDTO> getAllProducers() {
        List<Producer> producers = producerRepository.findAll();
        return this.producerMapper.maplist(producers, ProducerDTO.class);
    }

    @Override
    public boolean deleteProducer(Long id){
        this.producerRepository.delete(id);
        return true;
    }

}
