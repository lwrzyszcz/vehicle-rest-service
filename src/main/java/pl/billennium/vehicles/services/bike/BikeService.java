package pl.billennium.vehicles.services.bike;

import org.springframework.web.bind.annotation.PathVariable;
import pl.billennium.vehicles.dto.BikeDTO;

import java.util.List;

public interface BikeService {
    /**
     * This method will insert new bike with given number.
     * @param number
     * @return added Bike
     */
    BikeDTO putBikeWithNumber(@PathVariable Long number);

    /**
     *
     * @return All vehicles with type bike.
     */
    List<BikeDTO> getAllBikes();
}
