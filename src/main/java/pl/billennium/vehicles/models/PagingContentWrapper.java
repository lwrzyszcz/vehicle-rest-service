package pl.billennium.vehicles.models;


import lombok.Setter;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Iterator;
import java.util.List;

@Setter
public class PagingContentWrapper<T> implements Page<T> {

    private List<T> content;
    private int totalPages;
    private int totalElements;
    private int number;
    private PagingModel previous;
    private PagingModel next;

    @Override
    public int getTotalPages() {
        return totalPages;
    }

    @Override
    public long getTotalElements() {
        return totalElements;
    }

    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public int getSize() {
        return totalElements;
    }

    @Override
    public int getNumberOfElements() {
        return totalElements;
    }

    @Override
    public List<T> getContent() {
        return content;
    }

    @Override
    public boolean hasContent() {
        return content != null;
    }

    @Override
    public Sort getSort() {
        return null;
    }

    @Override
    public boolean isFirst() {
        return number == 0;
    }

    @Override
    public boolean isLast() {
        return number == totalPages;
    }

    @Override
    public boolean hasNext() {
        return number < totalPages;
    }

    @Override
    public boolean hasPrevious() {
        return number > 0;
    }

    @Override
    public Pageable nextPageable() {
        return next;
    }

    @Override
    public Pageable previousPageable() {
        return previous;
    }

    @Override
    public <S> Page<S> map(Converter<? super T, ? extends S> converter) {
        return null;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }
}
