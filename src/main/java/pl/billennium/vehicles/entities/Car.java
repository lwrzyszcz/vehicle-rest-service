package pl.billennium.vehicles.entities;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.billennium.vehicles.entities.converters.LocalDateToLongConverter;
import pl.billennium.vehicles.models.Color;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;
import java.time.LocalDate;
@Getter
@Setter
@NoArgsConstructor
@Entity
public class Car extends Vehicle {
    @Size(min=2, max=20)
    private String name;
    private Color color;
    @ManyToOne
    private Producer producer;
    @Convert(converter = LocalDateToLongConverter.class)
    private LocalDate productionDate;

    public Car(String name, Color myColor) {
        this.name = name;
        this.color = myColor;
    }

}