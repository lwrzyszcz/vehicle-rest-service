package pl.billennium.vehicles.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
@Getter
@Setter
@Entity
public class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToMany(mappedBy = "vehicle", cascade = CascadeType.ALL)
    private List<Borrow> borrows;
    @Column(insertable = false, updatable = false)
    private String dtype;

    public Vehicle(){}


}