package pl.billennium.vehicles.services.car;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import pl.billennium.vehicles.dto.CarDTO;
import pl.billennium.vehicles.dto.EditCarDTO;
import pl.billennium.vehicles.entities.Car;
import pl.billennium.vehicles.mappers.CarMapper;
import pl.billennium.vehicles.repositories.CarRepository;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private CarMapper carMapper;

    @Override
    public CarDTO addCar(@RequestBody CarDTO car) {
        Car mappedCar = carMapper.map(car, Car.class);
        Car addedCar = this.carRepository.save(mappedCar);
        return carMapper.map(addedCar, CarDTO.class);
    }

    @Override
    public CarDTO editCar(@RequestBody EditCarDTO car) {
        Car mappedCar = carMapper.map(car, Car.class);
        Car updatedCar = this.carRepository.save(mappedCar);
        return carMapper.map(updatedCar, CarDTO.class);
    }

    @Override
    public List<CarDTO> getAllCars() {
        List<Car> cars = carRepository.findAll();
        return carMapper.maplist(cars, CarDTO.class);
    }
}
