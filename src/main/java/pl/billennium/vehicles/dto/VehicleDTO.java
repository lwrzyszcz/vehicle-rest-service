package pl.billennium.vehicles.dto;

import lombok.Getter;
import lombok.Setter;
import pl.billennium.vehicles.models.Color;
@Getter
@Setter
public class VehicleDTO {

    private Long id;

    private Long bikeNumber;

    private String name;

    private Color color;

    private ProducerDTO producer;

    private BorrowDTO borrow;

    private String productionDate;

    private String type;

    private boolean borrowed = false;

}
